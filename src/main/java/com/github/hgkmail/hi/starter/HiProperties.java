package com.github.hgkmail.hi.starter;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = HiProperties.HI_PREFIX)
public class HiProperties {

    public static final String HI_PREFIX = "hgk.hi";

    private String who = "tom";

    private String greet = "hi";

    private Integer age = 20;

    public String getWho() {
        return who;
    }

    public void setWho(String who) {
        this.who = who;
    }

    public String getGreet() {
        return greet;
    }

    public void setGreet(String greet) {
        this.greet = greet;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "HiProperties{" +
                "who='" + who + '\'' +
                ", greet='" + greet + '\'' +
                ", age=" + age +
                '}';
    }
}
