package com.github.hgkmail.hi.starter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

@Configuration
@EnableConfigurationProperties(HiProperties.class)
public class HiAutoConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(HiAutoConfiguration.class);

    private HiService hiService;

    @Resource
    private HiProperties hiProperties;

    @Bean
    @ConditionalOnMissingBean(HiService.class)
    public HiService getHiService() {
        logger.info("配置信息: {}", hiProperties.toString());

        hiService = new HiService();
        hiService.setGreeting(String.format("%s %s %d",
                hiProperties.getGreet(), hiProperties.getWho(), hiProperties.getAge()));

        return hiService;
    }

}
