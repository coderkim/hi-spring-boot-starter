package com.github.hgkmail.hi.starter;

public class HiService {

    private String greeting;

    public String sayHi() {
        return greeting;
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
}
