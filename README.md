# hi-spring-boot-starter

#### 介绍

从零开始开发一个Spring Boot Starter  
参考文章：https://www.jianshu.com/p/bbf439c8a203  


#### 软件架构

1.  Spring Boot Starter是Spring Boot的重要组件机制，推荐使用maven来构建。  
2.  HiProperties类关联application.properties或application.yml的配置。  
3.  HiService是业务组件类。  
4.  HiAutoConfiguration是自动化配置类，从HiProperties类读取配置，创建并返回一个HiService业务类。  
5.  META-INF/spring.factories是元数据，列出有哪些自动化配置类。  


#### 安装教程

mvn install


#### 使用说明

1.  maven或gradle直接添加依赖  
2.  @Autowired直接使用HiService  


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

